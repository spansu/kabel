<div class="product__wrapper">
    <div class="container">
        <div class="row">
            <div class="twelve columns">
                <div class="breadcrumbs">
                    <a href="#">Главная</a>
                    <a href="#">Каталог</a>
                    <a href="#">Витая пара</a>
                    <a href="#">Кабель</a>
                    <a href="#">UTP 4 пары</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="three columns product__side-menu">
                <h3>Кабели и провода</h3>
                <ul class="product__side-menu__list">
                    <li><a href="#">(А)ПвБШв</a></li>
                    <li><a href="#">(А)ПвБШп</a></li>
                    <li><a href="#">(А)ПвБШп(Г)</a></li>
                    <li><a href="#">(А)ПвВнг(А)-LS</a></li>
                    <li><a href="#">(А)ПвВнг(В)-LS</a></li>
                    <li><a href="#">(А)ПвПг NUM NYM</a></li>
                    <li><a href="#">ААБл</a></li>
                    <li><a href="#">ААШв</a></li>
                    <li><a href="#">АВБбШв</a></li>
                    <li><a href="#">АВБбШвнг</a></li>
                    <li><a href="#">АВБбШвнг(А)</a></li>
                    <li><a href="#">АВБбШвнг(А)-LS</a></li>
                    <li><a href="#">АВВГ АВВГнг(А)</a></li>
                    <li><a href="#">АВВГнг(А)-LS</a></li>
                    <li><a href="#">АПвПуг</a></li>
                    <li><a href="#">АСБГ</a></li>
                    <li><a href="#">АСБл</a></li>
                    <li><a href="#">ВВГ</a></li>
                    <li><a href="#">ВВГнг</a></li>
                    <li><a href="#">ВВГнг-LS</a></li>
                    <li><a href="#">ВВГнг(А)</a></li>
                    <li><a href="#">ВВГнг(А)</a></li>
                    <li><a href="#">ВВГнг(А)-LS</a></li>
                    <li><a href="#">ВВГнг(А)-FRLS</a></li>
                    <li><a href="#">ВВГнг(А)-LS</a></li>
                    <li><a href="#">ВВГЭнг</a></li>
                    <li><a href="#">ВВГЭнг(А)-FRLS</a></li>
                    <li><a href="#">Кабель ВБбШв</a></li>
                    <li><a href="#">Кабель ВБбШвнг</a></li>
                    <li><a href="#">Кабель ВБбШвнг(А)</a></li>
                    <li><a href="#">Кабель ВБбШвнг(А)-FRLS</a></li>
                    <li><a href="#">Кабель ВБбШвнг(А)-LS</a></li>
                    <li><a href="#">Кабель ВКбШвнг(А)-LS</a></li>
                    <li><a href="#">КГ</a></li>
                    <li><a href="#">КГ-ХЛ</a></li>
                    <li><a href="#">КГН</a></li>
                    <li><a href="#">ПвБШв</a></li>
                    <li><a href="#">ПвБШп</a></li>
                    <li><a href="#">ППГнг-HF</a></li>
                    <li><a href="#">ППГнг(А)-FRHF</a></li>
                    <li><a href="#">ППГнг(А)-HF</a></li>
                </ul>
            </div>
            <div class="nine columns product__content__wrapper">
                <h1><span>Кабель КВВГНГ-FRLS 37*1,5</span></h1>
                <div class="product__content__inner">
                    <div class="product__info">
                        <div class="row">
                            <div class="six columns product-img__wrapper">
                                <div class="product-img"><img src="/img/new/product_card.jpg" alt="" /></div>
                            </div>
                            <div class="six columns product-descr__wrapper">
                                <div class="product-descr">
                                    <div class="product-available__wrapper">
                                        <div class="product-available__header">Наличие на складе <strong>КВВГНГ-FRLS 37*1,5</strong></div>
                                        <div class="product-available not-available">Есть в наличии!</div>
                                    </div>
            
                                    <div class="product-basket__wrapper">
                                        <form method="post" action="" class="product-basket__form">
                                            <div class="row">
                                                <div class="eight columns">
                                                    <div class="product-basket__header"><small>цена</small> КВВГНГ-FRLS 37*1,5 с НДС</div>
                                                    <div class="product-price__wrapper">
                                                        <div class="product-price">477,00 &#8381;</div>
                                                        <small>За метр погон.</small>
                                                    </div>
                                                    
                                                </div>
                                                <div class="four columns">
                                                    <div class="product-basket__buttons-wrapper">
                                                        <button class="onclick-by__btn">Купить в один клик</button>
                                                        <button class="product-basket__btn"><span>В корзину</span></button>

                                                        <div class="product-count__wrapper">
                                                            <div class="product-count__input__wrapper">
                                                                <a href="#">-</a><input type="text" value="1" /><a href="#">+</a>
                                                                <div class="units"><small>метр погон.</small></div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
    
                                    <div class="product__hint">
                                        <strong>Уважаемые посетители!</strong>
                                        <p>Стоимость товара может варьироваться в зависимости от курса евро или доллара. Пожалуйста, уточняйте актуальную стоимость у наших менеджеров.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product-tabs__wrapper">
                        <div class="product-tabs__links">
                            <ul>
                                <li class="active"><a href="#" data-tab="descr"><span>Описание</span></a></li>
                                <li><a href="#" data-tab="characteristics"><span>Характеристики</span></a></li>
                                <li><a href="#" data-tab="billing"><span>Оплата</span></a></li>
                                <li><a href="#" data-tab="delivery"><span>Доставка</span></a></li>
                            </ul>
                        </div>
                        <div class="product-tabs__content">
                            <div class="product-tabs__item active" data-tab="descr">
                                <p>
                                    <strong>Описание и расшифровка кабеля КВВГнг-FRLS 37х1,5:</strong><br />
                                    <strong>К</strong> - Кабель контрольный<br />
                                    <strong>В</strong> - Изоляция жил из поливинилхлоридного пластиката<br />
                                    <strong>В</strong> - Оболочка из поливинилхлоридного пластиката<br />
                                    <strong>Г</strong> - Отсутствие защитных покровов<br />
                                    <strong>нг-LS</strong> - Изоляция жил и оболочка из поливинилхлоридного пластиката пониженной горючести с пониженным газо- дымовыделением<br />
                                    <strong>FR</strong> - наличие термического барьера в виде обмотки проводника двумя слюдосодержащими лентами<br />
                                </p>
                                <p>
                                    <strong>Элементы конструкции кабеля КВВГнг-FRLS 37х1,5:</strong>
                                </p>
                                <ol>
                                    <li>Токопроводящая жила – из медной проволоки.</li>
                                    <li>Термический барьер - из слюдосодержащей ленты.</li>
                                    <li>Изоляция – из поливинилхлоридного пластиката пониженной пожароопасности.</li>
                                    <li>Внутренняя оболочка – из поливинилхлоридного пластиката пониженной пожароопасности.</li>
                                    <li>Разделительный слой из поливинилхлоридного пластиката пониженной пожароопасности.</li>
                                    <li>Экран - в виде обмотки из медной фольги или медной ленты номинальной толщиной не менее 0,06 мм с перекрытием не менее 30%,обеспечивающим сплошность экрана при допустимых радиусах изгиба кабеля.</li>
                                    <li>Наружная оболочка - из поливинилхлоридного пластиката пониженной пожароопасности.<br />
                                        Контрольные кабели имеют цифровую или цветную маркировку всех изолированных жил, обеспечивающую возможность идентификации каждой жилы при монтаже.</li>
                                </ol>
                            </div>
                            <div class="product-tabs__item" data-tab="characteristics">characteristics</div>
                            <div class="product-tabs__item" data-tab="billing">billing</div>
                            <div class="product-tabs__item" data-tab="delivery">delivery</div>
                        </div>
                    </div>

                    <div class="other-products">
                        <h4><span>Другие модификации провода КВВГНГ-FRLS в нашем каталоге продукции</span></h4>
                        <table class="other-products__table">
                            <thead>
                                <tr>
                                    <th>Наименование позиции</th>
                                    <th>Цена за пог. м</th>
                                    <th>Количество, м</th>
                                    <th>Заказать</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>КВВГНГ 2Х0.75</td>
                                    <td>9.56 руб.</td>
                                    <td>1</td>
                                    <td>
                                        <button class="add-to-order__btn">Добавить в заказ</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>КВВГНГ 2Х0.75</td>
                                    <td>9.56 руб.</td>
                                    <td>1</td>
                                    <td>
                                        <button class="add-to-order__btn">Добавить в заказ</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>КВВГНГ 2Х0.75</td>
                                    <td>9.56 руб.</td>
                                    <td>1</td>
                                    <td>
                                        <button class="add-to-order__btn">Добавить в заказ</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>КВВГНГ 2Х0.75</td>
                                    <td>9.56 руб.</td>
                                    <td>1</td>
                                    <td>
                                        <button class="add-to-order__btn">Добавить в заказ</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>КВВГНГ 2Х0.75</td>
                                    <td>9.56 руб.</td>
                                    <td>1</td>
                                    <td>
                                        <button class="add-to-order__btn">Добавить в заказ</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="viewed-products">
        <div class="container">
            <h3><span>Вы смотрели</span></h3>
            <div class="viewed-products__list">
                <div class="viewed-products__inner">
                    <?php for($i=0;$i<8;$i++){ ?>
                        <div class="viewed-products__list__item">
                            <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                            <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                            <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                            <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                            <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                        </div>
                    <?php } ?>
                </div>
                <a href="javascript:void(0)" class="viewed-products__list__arrow viewed-products__list__arrow-left fa fa-angle-left"></a>
                <a href="javascript:void(0)" class="viewed-products__list__arrow viewed-products__list__arrow-right fa fa-angle-right"></a>
            </div>
        </div>
    </div>


    <div class="partners__wrapper">
        <div class="container">
            <h3><span>Наши партнеры</span></h3>
            <div class="partners__inner">
                <div class="partners__slider">
                    <div class="partners__slider__inner">
                        <div class="partners__slider__item"><img src="/img/new/partner_amk.png" alt="" /></div>
                        <div class="partners__slider__item"><img src="/img/new/partner_legrand.png" alt="" /></div>
                        <div class="partners__slider__item"><img src="/img/new/partner_rubinsk.png" alt="" /></div>
                        <div class="partners__slider__item"><img src="/img/new/partner_sevkabel.png" alt="" /></div>
                        <div class="partners__slider__item"><img src="/img/new/partner_tatkabel.png" alt="" /></div>

                        <div class="partners__slider__item"><img src="/img/new/partner_amk.png" alt="" /></div>
                        <div class="partners__slider__item"><img src="/img/new/partner_legrand.png" alt="" /></div>
                        <div class="partners__slider__item"><img src="/img/new/partner_rubinsk.png" alt="" /></div>
                        <div class="partners__slider__item"><img src="/img/new/partner_sevkabel.png" alt="" /></div>
                        <div class="partners__slider__item"><img src="/img/new/partner_tatkabel.png" alt="" /></div>
                    </div>
                    <a href="javascript:void(0)" class="partners__slider__arrow partners__slider__arrow-left fa fa-angle-left"></a>
                    <a href="javascript:void(0)" class="partners__slider__arrow partners__slider__arrow-right fa fa-angle-right"></a>
                </div>
            </div>
        </div>
    </div>
</div>