<!-- Primary Page Layout
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="promo-block">
    <div class="container">
        <div class="row">
            <div class="twelve column">
                <div class="promo-block__slider-wrapper">
                    <div class="promo-block__slider">
                        <div class="promo-block__slider__slide">
                            <h3>
                                <span>У Вас еще нет кабеля?</span><br />
                                тогда мы идём к Вам!
                            </h3>
                            <a href="#" class="promo-block__slider__more">Подробнее</a>
                        </div>
                        <div class="promo-block__slider__slide">
                            <h3>
                                <span>1У Вас еще нет кабеля?</span><br />
                                тогда мы идём к Вам!
                            </h3>
                            <a href="#" class="promo-block__slider__more">Подробнее</a>
                        </div>
                        <div class="promo-block__slider__slide">
                            <h3>
                                <span>2У Вас еще нет кабеля?</span><br />
                                тогда мы идём к Вам!
                            </h3>
                            <a href="#" class="promo-block__slider__more">Подробнее</a>
                        </div>
                    </div>
                    <a href="javascript:void(0)" class="promo-block__slider__arrow promo-block__slider__arrow-left fa fa-angle-left"></a>
                    <a href="javascript:void(0)" class="promo-block__slider__arrow promo-block__slider__arrow-right fa fa-angle-right"></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="products-list__wrapper">
    <div class="container">
        <h3><span>Выгодное предложеие</span></h3>
        <div class="products-list">
            <div class="products-list__decorator products-list__decorator-top"></div>
            <div class="products-list__decorator products-list__decorator-bottom"></div>
            <div class="products-list__inner">
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
                <div class="products-list__item">
                    <div class="products-list__item__header">Кабель ПвПу2г 1х240/25 – 6/10кВ</div>
                    <div class="products-list__item__img"><img src="/img/new/product.png" alt="" /></div>
                    <div class="products-list__item__price-old">Старая цена: <span>550 000.00 P</span></div>
                    <div class="products-list__basket-btn-wrap"><button class="products-list__basket-btn"><span>388 000.00 P</span></button></div>
                    <div class="products-list__one-click-wrap"><a href="#" class="products-list__one-click">Купить в один клик</a></div>
                </div>
            </div>
        </div>
        <div class="products-list__more-wrap">
            <button class="products-list__more-btn"><span>показать еще +12</span></button>
        </div>
    </div>
</div>


<div class="categories-list__wrapper">
    <div class="container">
        <h3><span>Сопутствующие категории</span></h3>
        <div class="categories-list">
            <div class="categories-list__inner">
                <div class="categories-list__item">
                    <div class="categories-list__item__header">Кабельные муфты</div>
                    <div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
                    <div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
                    <div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
                </div>
                <div class="categories-list__item">
                    <div class="categories-list__item__header">Сетевое<br />оборудование</div>
                    <div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
                    <div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
                    <div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
                </div>
                <div class="categories-list__item">
                    <div class="categories-list__item__header">Кабельные муфты</div>
                    <div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
                    <div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
                    <div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
                </div>
                <div class="categories-list__item">
                    <div class="categories-list__item__header">Кабельные муфты</div>
                    <div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
                    <div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
                    <div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
                </div>
                <div class="categories-list__item">
                    <div class="categories-list__item__header">Кабельные муфты</div>
                    <div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
                    <div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
                    <div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
                </div>
                <div class="categories-list__item">
                    <div class="categories-list__item__header">Кабельные муфты</div>
                    <div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
                    <div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
                    <div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
                </div>
                <div class="categories-list__item">
                    <div class="categories-list__item__header">Кабельные муфты</div>
                    <div class="categories-list__item__img"><img src="/img/new/category.png" alt="" /></div>
                    <div class="categories-list__item__descr"><p>Кабельная муфта — предназна чена для соединения кабелей в кабельную линию.</p></div>
                    <div class="categories-list__btn-wrap"><a href="#" class="categories-list__btn"><span>Перейти</span></a></div>
                </div>
            </div>
            <a href="javascript:void(0)" class="categories-list__arrow categories-list__arrow-left fa fa-angle-left"></a>
            <a href="javascript:void(0)" class="categories-list__arrow categories-list__arrow-right fa fa-angle-right"></a>
        </div>
    </div>
</div>

<div class="work-shcheme__wrapper">
    <div class="container">
        <h3><span>Наша схема работы</span></h3>
        <div class="work-shcheme__inner">
            <p>Деятельность нашей компании ООО "Открытый Кабельный Портал", направлена на оптовые поставки кабельно-проводниковой продукции и электротехнических материалов на территории России и стран СНГ. Работая с нами, Вы тратите меньше - с нами выгодно.</p>
            <img src="/img/new/work-scheme.jpg" alt="" />
        </div>
    </div>
</div>


<div class="on-trust__wrapper">
    <div class="container">
        <h3><span>Нам доверяют</span></h3>
        <div class="on-trust">
            <div class="on-trust__inner">
                <div class="on-trust__item">
                    <div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
                    <div class="on-trust__item__header">
                        <span>ООО “ПерспективаЭллектро”</span>
                        Сидоров Н.А., директор
                    </div>
                </div>
                <div class="on-trust__item">
                    <div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
                    <div class="on-trust__item__header">
                        <span>ООО “ПерспективаЭллектро”</span>
                        Сидоров Н.А., директор
                    </div>
                </div>
                <div class="on-trust__item">
                    <div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
                    <div class="on-trust__item__header">
                        <span>ООО “ПерспективаЭллектро”</span>
                        Сидоров Н.А., директор
                    </div>
                </div>
                <div class="on-trust__item">
                    <div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
                    <div class="on-trust__item__header">
                        <span>ООО “ПерспективаЭллектро”</span>
                        Сидоров Н.А., директор
                    </div>
                </div>
                <div class="on-trust__item">
                    <div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
                    <div class="on-trust__item__header">
                        <span>ООО “ПерспективаЭллектро”</span>
                        Сидоров Н.А., директор
                    </div>
                </div>
                <div class="on-trust__item">
                    <div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
                    <div class="on-trust__item__header">
                        <span>ООО “ПерспективаЭллектро”</span>
                        Сидоров Н.А., директор
                    </div>
                </div>
                <div class="on-trust__item">
                    <div class="on-trust__item__img"><img src="/img/new/on-trust.jpg" alt="" /></div>
                    <div class="on-trust__item__header">
                        <span>ООО “ПерспективаЭллектро”</span>
                        Сидоров Н.А., директор
                    </div>
                </div>
            </div>
            <a href="javascript:void(0)" class="on-trust__arrow on-trust__arrow-left fa fa-angle-left"></a>
            <a href="javascript:void(0)" class="on-trust__arrow on-trust__arrow-right fa fa-angle-right"></a>
        </div>
    </div>
</div>

<div class="publications__wrapper">
    <div class="container">
        <div class="row">
            <div class="news six columns">
                <h3><span>Новости</span></h3>
                <div class="publication__inner">
                    <div class="publication__item">
                        <div class="publication__item__img"><a href="#"><img src="/img/new/publication.jpg" alt="" /></a></div>
                        <div class="publication__item__info">
                            <div class="publication__item__header"><a href="#">Власти Кувейта планирую развитие нефтяного сектора.</a></div>
                            <div class="publication__item__descr">До 2020 года для модернизации нефтяных заводов и увеличения уровня производства углеводородов на 1 млн баррелей в сутки будет выделено из бюджета $115 млрд (более 34 млрд динаров). Об этом передает корр. ТАСС.</div>
                            <div class="publication__read-more"><button class="publication__read-more-btn"><span>читать далее...</span></button></div>
                        </div>
                    </div>
                    <div class="publication__item">
                        <div class="publication__item__img"><a href="#"><img src="/img/new/publication.jpg" alt="" /></a></div>
                        <div class="publication__item__info">
                            <div class="publication__item__header"><a href="#">Власти Кувейта планирую развитие нефтяного сектора.</a></div>
                            <div class="publication__item__descr">До 2020 года для модернизации нефтяных заводов и увеличения уровня производства углеводородов на 1 млн баррелей в сутки будет выделено из бюджета $115 млрд (более 34 млрд динаров). Об этом передает корр. ТАСС.</div>
                            <div class="publication__read-more"><button class="publication__read-more-btn"><span>читать далее...</span></button></div>
                        </div>
                    </div>
                </div>
                <div class="publication__all-items"><button class="publication__all-items-btn"><span>Все новости</span></button></div>
            </div>

            <div class="articles six columns">
                <h3><span>Статьи</span></h3>
                <div class="publication__inner">
                    <div class="publication__item">
                        <div class="publication__item__img"><a href="#"><img src="/img/new/publication.jpg" alt="" /></a></div>
                        <div class="publication__item__info">
                            <div class="publication__item__header"><a href="#">Власти Кувейта планирую развитие нефтяного сектора.</a></div>
                            <div class="publication__item__descr">До 2020 года для модернизации нефтяных заводов и увеличения уровня производства углеводородов на 1 млн баррелей в сутки будет выделено из бюджета $115 млрд (более 34 млрд динаров). Об этом передает корр. ТАСС.</div>
                            <div class="publication__read-more"><button class="publication__read-more-btn"><span>читать далее...</span></button></div>
                        </div>
                    </div>
                    <div class="publication__item">
                        <div class="publication__item__img"><a href="#"><img src="/img/new/publication.jpg" alt="" /></a></div>
                        <div class="publication__item__info">
                            <div class="publication__item__header"><a href="#">Власти Кувейта планирую развитие нефтяного сектора.</a></div>
                            <div class="publication__item__descr">До 2020 года для модернизации нефтяных заводов и увеличения уровня производства углеводородов на 1 млн баррелей в сутки будет выделено из бюджета $115 млрд (более 34 млрд динаров). Об этом передает корр. ТАСС.</div>
                            <div class="publication__read-more"><button class="publication__read-more-btn"><span>читать далее...</span></button></div>
                        </div>
                    </div>
                </div>
                <div class="publication__all-items"><button class="publication__all-items-btn"><span>Все статьи</span></button></div>
            </div>
        </div>
    </div>
</div>


<div class="reply__wrapper">
    <div class="container">
        <h3><span>Напишите нам</span></h3>
        <div class="reply-form">
            <div class="reply__discount-msg">
                <p>Хотите получить скидку в <span>5%</span>? - оставьте заявку онлайн<br />
                    и мы подготовим для Вас индивидуальное, выгодное предложение!</p>
            </div>
            <form method="post" action="">
                <div class="row">
                    <div class="four columns">
                        <div class="reply-form__input-wrapper"><input type="text" placeholder="Ваше имя" value="" /></div>
                        <div class="reply-form__input-wrapper"><input type="text" placeholder="Ваше email" value="" /></div>
                        <div class="reply-form__input-wrapper"><input type="text" placeholder="Ваше телефон" value="" /></div>
                    </div>
                    <div class="four columns">
                        <div class="reply-form__input-wrapper"><textarea placeholder="Введите текст Вашего сообщения..."></textarea></div>
                    </div>
                    <div class="four columns">
                        <div class="reply-form__input-wrapper">
                            <div class="reply-form__file-input-wrapper"><input type="file" value="" /></div>
                            <div class="reply-form__hint">
                                Вы можете прикрепть файл не более 10 Мб,<br />
                                формата  .jpg, .png. .doc. .exls, .pdf, .rar, .zip
                            </div>
                        </div>
                        <div class="reply-form__submit-wrapper">
                            <button type="submit" class="reply-form__submit-btn"><span>Отправить</span></button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="twelve columns">
                        <div class="reply-form__hint tex">
                            <span>*</span> Мы не распространяем Ваши данные третьим лицам и не используем Ваши конткты для различных рассылок и т.д.
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="partners__wrapper">
    <div class="container">
        <h3><span>Наши партнеры</span></h3>
        <div class="partners__inner">
            <div class="partners__slider">
                <div class="partners__slider__inner">
                    <div class="partners__slider__item"><img src="/img/new/partner_amk.png" alt="" /></div>
                    <div class="partners__slider__item"><img src="/img/new/partner_legrand.png" alt="" /></div>
                    <div class="partners__slider__item"><img src="/img/new/partner_rubinsk.png" alt="" /></div>
                    <div class="partners__slider__item"><img src="/img/new/partner_sevkabel.png" alt="" /></div>
                    <div class="partners__slider__item"><img src="/img/new/partner_tatkabel.png" alt="" /></div>

                    <div class="partners__slider__item"><img src="/img/new/partner_amk.png" alt="" /></div>
                    <div class="partners__slider__item"><img src="/img/new/partner_legrand.png" alt="" /></div>
                    <div class="partners__slider__item"><img src="/img/new/partner_rubinsk.png" alt="" /></div>
                    <div class="partners__slider__item"><img src="/img/new/partner_sevkabel.png" alt="" /></div>
                    <div class="partners__slider__item"><img src="/img/new/partner_tatkabel.png" alt="" /></div>
                </div>
                <a href="javascript:void(0)" class="partners__slider__arrow partners__slider__arrow-left fa fa-angle-left"></a>
                <a href="javascript:void(0)" class="partners__slider__arrow partners__slider__arrow-right fa fa-angle-right"></a>
            </div>
        </div>
    </div>
</div>

<div class="about__wrapper">
    <div class="container">
        <h3><span>О нас</span></h3>
        <div class="about__inner">
            <div class="about__text">
                <img class="about__img" src="/img/new/about_img.jpg" alt="" />
                <p>
                    Компания <strong>Открытый Кабельный Портал</strong> занимается комплексными оптовыми поставками кабельной продукции для предприятий России, Белоруссии, Украины и Казахстана. Мы поставляем силовой, слаботочный, волоконно-оптический, коаксиальный кабель, полный спектр электротехнической продукции (светильники, щиты, автоматы, переключатели). Осуществляем сборку щитов.
                <p>
                    Нашими поставщиками являются кабельные заводы Российской Федерации и стран СНГ (РыбинскКабель,  ИркутскКабель, ПсковКабель, ТатКабель, Паритет, СПКБ, СпецКабель, Арсенал, СпецКабель, СПКБ, и некоторые небольшие производства).
                </p>
                <p>
                    У нас есть кабель в наличии, если же вам потребуется что-то особенное, то всегда можно оформить заказ. Благодаря налаженным связям и длительным партнерским отношениям, Открытый Кабельный Портал имеет дилерские скидки на заводах, а следовательно и отличные цены для наших партнеров.Нашим клиентами являются электромонтажные, строительные организации, провайдеры сети Интернет, а также торговые организации. Комплексная поставка на объекты нефте-газовой промышленности. Монтажным организациям по всей территории страны.
                </p>
                <p>
                    Главная цель нашей компании, это снабжение и логистика, на предприятиях и организациях по территории России, в таких городах как: Москва, Санкт-Петербург, Воронеж, Курск, Белгород, Тула, Архангельск, Калининград, Мурманск, Ростов, Ставрополь, Краснодар, Волгоград, Пермь, республика Татарстан, Саратов, республика Башкортостан, Оренбург, Самара, Свердловск, Астрахань, Челябинск, Тюмень, Томск, Новосибирск, Кемерово, Хабаровск, Новый Уренгой. Мы предлагаем оптовую цену, кабель из наличия, доставку кабеля в регионы страны.
                </p>
                <p>
                    Если Вам нужен кабель оптом или оптовая цена, то Вы можете обратиться в наш отдел продаж, наши специалисты проконсультируют по подбору нужной продукции и мы знаем как важны сроки поставки, поэтому никогда не подводим своих клиентов. А тот факт, что наши цены ниже чем у конкурентов, делает наших клиентов самыми счастливыми людьми на свете.
                </p>
            </div>
        </div>
    </div>
</div>