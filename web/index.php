<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>Your page title here :)</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FONT
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
    <link href="/css/new/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" href="/css/new/normalize.css">
    <link rel="stylesheet" href="/css/new/skeleton.css">
    <link rel="stylesheet" href="/css/new/skeleton.custom.css">
    <link rel="stylesheet" href="/css/new/icons.css?r=1">
    <link rel="stylesheet" href="/js/new/slick/slick.css">
    <link rel="stylesheet" href="/js/new/slick/slick-theme.css">
    <link rel="stylesheet" href="/css/new/main.css?r=4">

    <!-- Favicon
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="icon" type="image/png" href="images/favicon.png">


    <!-- JS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <script type="text/javascript" src="//code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="/js/new/slick/slick.min.js"></script>
    <script type="text/javascript" src="/js/new/main.js?r=4"></script>

</head>
<body>
    <header class="site-header">
        <div class="site-header__top collapsed">

            <div class="container">

                <div class="site-header__top__block site-header__schedule myicon myicon__schedule">
                    <span class="site-header__schedule__days site-header__hlight">Пн.-Птн.:</span>
                    <span class="site-header__schedule__time">с 09:00 до 18:00</span>
                </div>

                <a href="#" class="site-header__top__block site-header__region myicon myicon__map-marker">
                    <span class="site-header__region__caption site-header__hlight">Ваш регион:</span>
                    <span class="site-header__region__name">Санкт-Петербург</span>
                </a>

                <div class="site-header__top__block site-header__search">
                    <form method="post" action="" class="site-header__search__form">
                        <input class="site-header__search__input" type="text" name="search" value="" placeholder="Поиск..." />
                        <button class="site-header__search__btn myicon myicon__search" type="submit"></button>
                    </form>
                </div>

                <a href="#" class="site-header__top__block site-header__basket myicon myicon__basket">
                    <span class="site-header__basket__caption">Товара</span>
                    <span class="site-header__basket__products-count">(0)</span>
                </a>

                <a href="#" class="site-header__top__block site-header__signin myicon myicon__signin">
                    <span class="site-header__signin__caption">Войти</span>
                </a>

                <a href="#" class="site-header__top__menu"></a>

            </div>

        </div>
        <div class="site-header__middle">
            <div class="container">
                <a href="/" class="site-header__middle__block site-header__logo">Открытый кабельный портал</a>

                <div class="site-header__middle__block site-header__intro">
                    <p>
                        Оптовые поставки кабельно-проводниковой<br />
                        продукции и электротехнических материалов<br />
                        на территории России и стран СНГ
                    </p>
                </div>

                <div class="site-header__middle__block site-header__contacts">
                    <div class="site-header__email myicon myicon__email">info@cable-operator.ru</div>
                    <div class="site-header__phone"><span class="city">СПб:</span> <span class="phone-number">+7 (812) 309-85-60</span></div>
                    <div class="site-header__phone"><span class="city">Мск.:</span> <span class="phone-number">+7 (499) 703-44-56</span></div>
                    <div class="site-header__phone"><span class="city">Мур.:</span> <span class="phone-number">+7 (815) 259-64-39</span></div>
                    <button class="site-header__callback myicon myicon__callback">Вам перезвонить?</button>
                </div>
            </div>
        </div>
        <div class="site-header__bottom">
            <div class="container">
                <ul class="main-menu">
                    <li class="active"><a href="#">Главная</a></li>
                    <li><a href="#">Каталог</a></li>
                    <li><a href="#">О компании</a></li>
                    <li><a href="#">Доставка</a></li>
                    <li><a href="#">Новости</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </div>
        </div>
    </header>


<section class="site-content">
    <?php
        $page = isset($_GET['page'])? $_GET['page']:'homepage';
        $pages = [
            'homepage' => '_homepage.php',
            'catalog' => '_catalog.php',
            'product' => '_product.php',
        ];
        if(isset($pages[$page])){
            include $pages[$page];
        }
    ?>
</section>

    <footer class="site-footer">
        <div class="site-footer__top site-footer__delivery">
            <div class="container">
                <div class="row">
                    <div class="six columns site-footer__delivery__item wicon wicon__delivery">
                        <span>Бесплатная доставка</span>
                        <small>В пределах КАД</small>
                    </div>
                    <div class="six columns site-footer__delivery__item wicon wicon__phone">
                        <span>8-800-350-00-21</span>
                        <small>Бесплатно по РФ</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="site-footer__middle">
            <div class="container">
                <div class="row">
                    <div class="three columns site-footer__middle__block">
                        <h4><span>Разделы сайта</span></h4>
                        <ul class="site-footer__menu">
                            <li><a href="#">Главная</a></li>
                            <li><a href="#">Каталог</a></li>
                            <li><a href="#">О компании</a></li>
                            <li><a href="#">Доставка</a></li>
                            <li><a href="#">Новости</a></li>
                            <li><a href="#">Статьи</a></li>
                            <li><a href="#">Заявка онлайн</a></li>
                            <li><a href="#">Сотрудничество</a></li>
                            <li><a href="#">Вакансии</a></li>
                            <li><a href="#">Расчёт загрузки</a></li>
                            <li><a href="#">Справочная</a></li>
                            <li><a href="#">Возврат и обмен</a></li>
                            <li><a href="#">Карта сайта</a></li>
                            <li><a href="#">Контакты</a></li>
                        </ul>
                    </div>
                    <div class="three columns site-footer__middle__block">
                        <div class="site-footer__soc">
                            <h4><span>Мы в соц сетях</span></h4>
                            <ul class="site-footer__soc__list">
                                <li><a href="#" class="wicon wicon__google"></a></li>
                                <li><a href="#" class="wicon wicon__tw"></a></li>
                                <li><a href="#" class="wicon wicon__mail"></a></li>
                                <li><a href="#" class="wicon wicon__fb"></a></li>
                                <li><a href="#" class="wicon wicon__rss"></a></li>
                                <li><a href="#" class="wicon wicon__vk"></a></li>
                                <li><a href="#" class="wicon wicon__a"></a></li>
                                <li><a href="#" class="wicon wicon__ya"></a></li>
                                <li><a href="#" class="wicon wicon__pencil"></a></li>
                                <li><a href="#" class="wicon wicon__ok"></a></li>
                            </ul>
                        </div>

                        <div class="site-footer__pay">
                            <h4><span>Мы принимаем</span></h4>
                            <ul class="site-footer__pay__list">
                                <li><a href="#" class="wicon wicon__visa"></a></li>
                                <li><a href="#" class="wicon wicon__master-card"></a></li>
                                <li><a href="#" class="wicon wicon__maestro"></a></li>
                                <li><a href="#" class="wicon wicon__wm"></a></li>
                                <li><a href="#" class="wicon wicon__pay-pal"></a></li>
                                <li><a href="#" class="wicon wicon__liqpay"></a></li>
                                <li><a href="#" class="wicon wicon__yad"></a></li>
                                <li><a href="#" class="wicon wicon__contact"></a></li>
                                <li><a href="#" class="wicon wicon__qiwi"></a></li>
                                <li><a href="#" class="wicon wicon__sms"></a></li>
                                <li><a href="#" class="wicon wicon__pochta-rf"></a></li>
                                <li><a href="#" class="wicon wicon__rbk"></a></li>
                            </ul>
                        </div>

                        <div class="site-footer__subscribe">
                            <h4><span>Будь вкурсе</span></h4>
                            <form action="" method="post" class="subscribe-form">
                                <div class="subscribe-form__input-wrapper">
                                    <input type="text" value="" />
                                    <button type="submit" class="wicon wicon__email"></button>
                                </div>
                                <div class="subscribe-form__hint">
                                    Оставляя свой email адрес Вы автоматически даёте согласие на получение писем с акциями и новостями от нас.
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="three columns site-footer__middle__block site-footer__offices__wrap">
                        <div class="site-footer__offices">
                            <h4><span>Наши адреса</span></h4>
                            <div class="site-footer__offices__item">
                                <div class="site-footer__offices__item-inner wicon wicon__location">
                                    <div class="city">г. Санкт-Петербург</div>
                                    <div class="address">Московское шоссе, 25к1 Б/Ц "ПРЕСТИЖ»</div>
                                    <a href="#">Смотреть на карте</a>
                                </div>
                            </div>

                            <div class="site-footer__offices__item">
                                <div class="site-footer__offices__item-inner wicon wicon__location">
                                    <div class="city">г. Москва</div>
                                    <div class="address">Щербинка Южная 10 офис 6</div>
                                    <a href="#">Смотреть на карте</a>
                                </div>
                            </div>

                            <div class="site-footer__offices__item">
                                <div class="site-footer__offices__item-inner wicon wicon__location">
                                    <div class="city">г. Мурманск</div>
                                    <div class="address">Домостроительная улица, 2</div>
                                    <a href="#">Смотреть на карте</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="three columns site-footer__middle__block">
                        <div class="site-footer__contacts">
                            <h4><span>Контакты</span></h4>
                            <div class="site-footer__contacts__item">
                                <div class="site-footer__contacts__item-inner wicon wicon__phone-white">
                                    <div class="phone">+ 7 (812) 309 85 60</div>
                                    <div class="city">г. Санкт-Петербург</div>
                                    <a href="mailto:info@cable-operator.ru">info@cable-operator.ru</a>
                                </div>
                            </div>

                            <div class="site-footer__contacts__item">
                                <div class="site-footer__contacts__item-inner wicon wicon__phone-white">
                                    <div class="phone">+ 7 (499) 703 44 56</div>
                                    <div class="city">г. Москва</div>
                                    <a href="mailto:info@cable-operator.ru">info@cable-operator.ru</a>
                                </div>
                            </div>

                            <div class="site-footer__contacts__item">
                                <div class="site-footer__contacts__item-inner wicon wicon__phone-white">
                                    <div class="phone">+ 7 (8152) 59 64 39</div>
                                    <div class="city">г. Мурманск</div>
                                    <a href="mailto:info@cable-operator.ru">info@cable-operator.ru</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
